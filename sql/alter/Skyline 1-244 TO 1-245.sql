SET foreign_key_checks = 0;
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.244');

# ---------------------------------------------------------------------- #
# Part Stock Tables                                                      #
# ---------------------------------------------------------------------- # 
CREATE TABLE `sp_part_status` ( `SPPartStatusID` INT(11) NOT NULL AUTO_INCREMENT
, `Description` INT(11) NOT NULL
, `Available` ENUM('Y','N') NOT NULL
, `InStock` ENUM('Y','N') NOT NULL, 
PRIMARY KEY (`SPPartStatusID`) 
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE `sp_part_stock_history` (
`SPPartStockHistoryID` INT(11) NOT NULL AUTO_INCREMENT, 
`SPPartStockItemID` INT(11) NOT NULL, 
`SPPartStatusID` INT(11) NOT NULL, 
`UserID` INT(11) NOT NULL
, `DateTime` DATETIME NOT NULL, 
`Reference` VARCHAR(50) NULL DEFAULT NULL, 
PRIMARY KEY (`SPPartStockHistoryID`), 
INDEX `SPPartStockItemID` (`SPPartStockItemID`), 
INDEX `SPPartStatusID` (`SPPartStatusID`), 
INDEX `user_TO_sp_part_stock_history` (`UserID`), 
CONSTRAINT `sp_part_statusTO_sp_part_stock_history` FOREIGN KEY (`SPPartStatusID`) REFERENCES `sp_part_status` (`SPPartStatusID`), 
CONSTRAINT `sp_part_stock_item_TO_sp_part_stock_history` FOREIGN KEY (`SPPartStockItemID`) REFERENCES `sp_part_stock_item` (`SPPartStockItem`), 
CONSTRAINT `user_TO_sp_part_stock_history` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) 
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE `sp_part_stock_item` (
`SPPartStockItem` INT(11) NOT NULL AUTO_INCREMENT,
`SPPartStockTemplateID` INT(11) NOT NULL,
`SPPartStatusID` INT(11) NOT NULL,
`JobID` INT(11) NULL DEFAULT NULL,
`Serial` VARCHAR(30) NULL DEFAULT NULL,
`PurchaseCost` DECIMAL(10,2) NULL DEFAULT NULL,
`ModifiedByUser` INT(10) NOT NULL,
`ModifiedDate` INT(10) NOT NULL,
PRIMARY KEY (`SPPartStockItem`),
INDEX `SPPartStockTemplateID` (`SPPartStockTemplateID`),
INDEX `SPPartStatusID` (`SPPartStatusID`),
INDEX `JobID` (`JobID`),
INDEX `Serial` (`Serial`),
CONSTRAINT `sp_part_status_TO_sp_part_stock_item` FOREIGN KEY (`SPPartStatusID`) REFERENCES `sp_part_status` (`SPPartStatusID`),
CONSTRAINT `job_TO_sp_part_stock_item` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

CREATE TABLE sp_part_stock_template (
SpPartStockTemplateID INT(10) NOT NULL AUTO_INCREMENT, 
PartNumber VARCHAR(20) NULL DEFAULT NULL, 
PurchaseCost DECIMAL(10,4) NULL DEFAULT NULL, 
Description VARCHAR(50) NULL DEFAULT NULL, 
ServiceProviderColourID INT(11) NULL DEFAULT NULL, 
PrimaryServiceProviderModelID INT(11) NULL DEFAULT NULL, 
MinStock INT(11) NULL DEFAULT NULL, 
MakeUpTo INT(11) NULL DEFAULT NULL, 
Status ENUM('Active','In-active') NULL DEFAULT 'Active', 
CreatedDate TIMESTAMP NULL DEFAULT NULL, 
CreatedUserID INT(11) NULL DEFAULT NULL, 
ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
ModifiedUserID INT(11) NULL DEFAULT NULL, 
ServiceProviderID INT(11) NULL DEFAULT NULL, 
PRIMARY KEY (SpPartStockTemplateID)
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.245');

SET foreign_key_checks = 1;
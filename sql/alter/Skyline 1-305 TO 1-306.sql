# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.305');

# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE report_log CHANGE COLUMN ReportDescription ReportDescription TEXT NULL DEFAULT NULL AFTER ReportName, CHANGE COLUMN Recipients Recipient VARCHAR(100) NULL DEFAULT NULL AFTER ReportDescription;

# ---------------------------------------------------------------------- #
# Krishnam Raju Nalla Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE remote_engineer_history CHANGE Latitude Latitude DOUBLE NULL DEFAULT NULL , CHANGE Longitude Longitude DOUBLE NULL DEFAULT NULL;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.306');




# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.221');


# ---------------------------------------------------------------------- #
# Add Table supplier                                                     #
# ---------------------------------------------------------------------- #
CREATE TABLE supplier (
						SupplierID INT(10) NOT NULL AUTO_INCREMENT, 
						CompanyName VARCHAR(50) NOT NULL, 
						AccountNumber VARCHAR(50) NULL DEFAULT NULL, 
						TelephoneNo VARCHAR(20) NULL DEFAULT NULL, 
						PostCode VARCHAR(10) NULL DEFAULT NULL, 
						FaxNo VARCHAR(13) NULL DEFAULT NULL, 
						EmailAddress VARCHAR(255) NULL DEFAULT NULL, 
						Website VARCHAR(255) NULL DEFAULT NULL, 
						ContactName VARCHAR(50) NULL DEFAULT NULL, 
						DirectOrderTemplateID INT(11) NULL DEFAULT NULL, 
						CollectSupplierOrderNo VARCHAR(20) NULL DEFAULT NULL, 
						PostageChargePrompt ENUM('Yes','No') NULL DEFAULT NULL, 
						DefaultPostageCharge DECIMAL(10,2) NULL DEFAULT NULL, 
						DefaultCurrencyID INT(11) NULL DEFAULT NULL, 
						NormalSupplyPeriod INT(11) NULL DEFAULT NULL, 
						MinimumOrderValue DECIMAL(50,0) NULL DEFAULT NULL, 
						UsageHistoryPeriod INT(50) NULL DEFAULT NULL, 
						MultiplyByFactor DECIMAL(10,2) NULL DEFAULT NULL, 
						TaxExempt ENUM('Yes','No') NULL DEFAULT NULL, 
						OrderPermittedVariancePercent DECIMAL(10,2) NULL DEFAULT NULL, 
						Status ENUM('Active','In-Active') NOT NULL DEFAULT 'Active', 
						PRIMARY KEY (SupplierID), 
						INDEX CompanyName (CompanyName), 
						INDEX AccountNumber (AccountNumber) 
						) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Add Table datatables_custom_default_columns                            #
# ---------------------------------------------------------------------- #						
ALTER TABLE datatables_custom_default_columns CHANGE COLUMN ColumnNameString ColumnNameString VARCHAR(5000) NULL DEFAULT NULL AFTER ColumnOrderString, 
											  ADD COLUMN ColumnStatusString VARCHAR(500) NULL DEFAULT NULL AFTER ColumnNameString;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.222');

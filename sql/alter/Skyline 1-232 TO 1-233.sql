# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.232');


# ---------------------------------------------------------------------- #
# Service Base On Skyline Changes for Vic                                #
# ---------------------------------------------------------------------- # 

SET foreign_key_checks = 0;

CREATE TABLE IF NOT EXISTS `condition_code` (
  `ConditionCodeID` int(11) NOT NULL AUTO_INCREMENT,
  `ConditionCode` char(1) NOT NULL,
  `ConditionCodeDescription` varchar(100) NOT NULL,
  `ConditionCodeComment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ConditionCodeID`),
  KEY `user_TO_condition_codes` (`ModifiedUserID`),
  CONSTRAINT `user_TO_condition_codes` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `condition_code` (`ConditionCodeID`, `ConditionCode`, `ConditionCodeDescription`, `ConditionCodeComment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '1', 'Constantly', 'constant faults', 1, '2013-04-19 11:25:55'),
	(2, '2', 'Intermittently', 'intermittent faults', 1, '2013-04-19 11:25:55'),
	(3, 'A', 'Due to physical damage', 'fault was due to customer misuse', 1, '2013-04-19 11:25:55'),
	(4, 'C', 'Only on certain networks', 'is the fault only occuring on particular networks/services i.e. 3G', 1, '2013-04-19 11:25:55'),
	(5, 'H', 'In standby/idle mode', 'fault occurs when no fuctions/features are being activated', 1, '2013-04-19 11:25:55'),
	(6, 'L', 'Liquid contamination', 'fault was due to liquid damage', 1, '2013-04-19 11:25:55'),
	(7, 'Q', 'only while sending/writing', 'fault occurs when making calls or sending/writing SMS/MMS', 1, '2013-04-19 11:25:55'),
	(8, 'R', 'only while receiving/reading', 'fault occurs when answering call or receiving/reading SMS/MMS', 1, '2013-04-19 11:25:55');


CREATE TABLE IF NOT EXISTS `condition_code_subset` (
  `ConditionCodeID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `RepairSkillID` int(11) NOT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `manufacturer_TO_condition_codes` (`ManufacturerID`),
  KEY `repair_skill_TO_condition_codes` (`RepairSkillID`),
  KEY `user_TO_condition_codes` (`ModifiedUserID`),
  KEY `condition_code_TO_condition_code_subset` (`ConditionCodeID`),
  CONSTRAINT `condition_code_TO_condition_code_subset` FOREIGN KEY (`ConditionCodeID`) REFERENCES `condition_code` (`ConditionCodeID`),
  CONSTRAINT `manufacturer_TO_condition_code_subset` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_condition_code_subset` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_condition_code_subset` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT IGNORE INTO `condition_code_subset` (`ConditionCodeID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 106, 2, 1, '2013-04-19 11:25:55'),
	(2, 106, 2, 1, '2013-04-19 11:25:55'),
	(3, 106, 2, 1, '2013-04-19 11:25:55'),
	(4, 106, 2, 1, '2013-04-19 11:25:55'),
	(5, 106, 2, 1, '2013-04-19 11:25:55'),
	(6, 106, 2, 1, '2013-04-19 11:25:55'),
	(7, 106, 2, 1, '2013-04-19 11:25:55'),
	(8, 106, 2, 1, '2013-04-19 11:25:55');


CREATE TABLE IF NOT EXISTS `symptom_code` (
  `SymptomCodeID` int(11) NOT NULL AUTO_INCREMENT,
  `SymptomCode` char(3) NOT NULL,
  `SymptomCodeDescription` varchar(250) DEFAULT NULL,
  `SymptomCodeComment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SymptomCodeID`),
  KEY `user_TO_symptom_codes` (`ModifiedUserID`),
  CONSTRAINT `user_TO_symptom_codes` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `symptom_code` (`SymptomCodeID`, `SymptomCode`, `SymptomCodeDescription`, `SymptomCodeComment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '110', 'No power on', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(2, '11A', 'Powers up but no operation', 'handset freezes when/after powering up', 1, '2013-04-19 11:18:42'),
	(3, '11B', 'Cyclic power on/off', 'handset resets', 1, '2013-04-19 11:18:42'),
	(4, '11D', 'Set switches off by itself', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(5, '11J', 'Battery not recognised', 'customers/handset battery at fault and replaced', 1, '2013-04-19 11:18:42'),
	(6, '117', 'Short operating time/short battery life', '', 1, '2013-04-19 11:18:42'),
	(7, '120', 'Charging problem', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(8, '121', 'No charging battery', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(9, '28B', 'Faulty handsfree operation', 'PHF at fault', 1, '2013-04-19 11:18:42'),
	(10, '210', 'No reception', 'no signal strength/indicator', 1, '2013-04-19 11:18:42'),
	(11, '220', 'Poor reception', 'low signal strength/indicator', 1, '2013-04-19 11:18:42'),
	(12, '231', 'No transmission', 'cannot make outgoing calls', 1, '2013-04-19 11:18:42'),
	(13, '131', 'No display', 'blank/dead screen output', 1, '2013-04-19 11:18:42'),
	(14, '139', 'Display dim/too dark', 'contrast faults', 1, '2013-04-19 11:18:42'),
	(15, '13F', 'Missing segments', 'pixels/segments missing', 1, '2013-04-19 11:18:42'),
	(16, '13B', 'No backlight', 'no LCD LED operation', 1, '2013-04-19 11:18:42'),
	(17, '13G', 'Problem with ambient light', 'ambient light sensor at fault', 1, '2013-04-19 11:18:42'),
	(18, '382', 'Scratch on display', 'for refurb use only as per requirements by networks', 1, '2013-04-19 11:18:42'),
	(19, '741', 'Touchscreen not working', 'Touchscreen does not respond to any presses', 1, '2013-04-19 11:18:42'),
	(20, '742', 'Touch screen lock up', 'Touchscreen does work but locks up after a few presses', 1, '2013-04-19 11:18:42'),
	(21, '749', 'Touch screen surface damaged', 'Surface damage is preventing normal operation (also use for refurb)', 1, '2013-04-19 11:18:42'),
	(22, '17F', 'Faulty switch/button', 'intermittent and constant faults all keys', 1, '2013-04-19 11:18:42'),
	(23, '745', 'Inoperative keys', 'none of the keypad functions', 1, '2013-04-19 11:18:42'),
	(24, '132', 'Faulty LED operation', 'no keypad LED operation', 1, '2013-04-19 11:18:42'),
	(25, '277', 'No ringing tone', 'cannot hear handset ringtones', 1, '2013-04-19 11:18:42'),
	(26, '514', 'No audio playback', 'MP3 audio fault', 1, '2013-04-19 11:18:42'),
	(27, '516', 'No sound from speaker', 'loudspeaker faults', 1, '2013-04-19 11:18:42'),
	(28, '517', 'No sound from earphone/headphone', 'earpiece/headset faults', 1, '2013-04-19 11:18:42'),
	(29, '518', 'No microphone sound', 'no outgoing audio', 1, '2013-04-19 11:18:42'),
	(30, '532', 'Distorted audio', 'poor quality audio from earpiece, mic, loudspeaker or headsets', 1, '2013-04-19 11:18:42'),
	(31, '670', 'Mechanical operation problem', 'slider/flip mechanical operation faults', 1, '2013-04-19 11:18:42'),
	(32, '16E', 'External surface damage', 'paint peeling, plastic cracking - not caused by customer misuse', 1, '2013-04-19 11:18:42'),
	(33, '37D', 'Faulty picture capture function', 'cannot take pictures or make videos', 1, '2013-04-19 11:18:42'),
	(34, '788', 'No Bluetooth/WiFi/IR', 'can\'t find for other Bluetooth/WiFi/IR Device', 1, '2013-04-19 11:18:42'),
	(35, '785', 'Incompatible with other system', 'Bluetooth not working with handsfree/other devices', 1, '2013-04-19 11:18:42'),
	(36, '21B', 'No infrared reception/emission', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(37, '212', 'No FM Reception', 'FM radio feature does not work', 1, '2013-04-19 11:18:42'),
	(38, '160', 'Physical damage', 'Damaged caused by customer misuse - only for BER claims', 1, '2013-04-19 11:18:42'),
	(39, '171', 'Faulty clock function', 'issues with clock or alarm', 1, '2013-04-19 11:18:42'),
	(40, '181', 'Test and Check', 'NFF when no customer symptom is reported', 1, '2013-04-19 11:18:42'),
	(41, '18C', 'Software upgrade requested', 'Customer/network has requested latest approved SW to be loaded', 1, '2013-04-19 11:18:42'),
	(42, '717', 'No data storage', 'cannot read write to memory card or HDD (i.e. SGH-i300)', 1, '2013-04-19 11:18:42'),
	(43, '718', 'No data communication', 'WAP/GPRS/3G call faults', 1, '2013-04-19 11:18:42'),
	(44, '76C', 'Insert SIM card', 'handset does not recognise SIM card inserted', 1, '2013-04-19 11:18:42');


CREATE TABLE IF NOT EXISTS `symptom_code_subset` (
  `SymptomCodeID` int(11) NOT NULL,
  `SymptomTypeID` int(11) DEFAULT NULL,
  `SymptomConditionID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `symptom_code_TO_symptom_code_subset` (`SymptomCodeID`),
  KEY `symptom_type_TO_symptom_code_subset` (`SymptomTypeID`),
  KEY `symptom_condition_TO_symptom_code_subset` (`SymptomConditionID`),
  KEY `manufacturer_TO_symptom_code_subset` (`ManufacturerID`),
  KEY `repair_skill_TO_symptom_code_subset` (`RepairSkillID`),
  KEY `user_TO_symptom_code_subset` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_symptom_code_subset` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_symptom_code_subset` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `symptom_code_TO_symptom_code_subset` FOREIGN KEY (`SymptomCodeID`) REFERENCES `symptom_code` (`SymptomCodeID`),
  CONSTRAINT `symptom_condition_TO_symptom_code_subset` FOREIGN KEY (`SymptomConditionID`) REFERENCES `symptom_condition` (`SymptomConditionID`),
  CONSTRAINT `symptom_type_TO_symptom_code_subset` FOREIGN KEY (`SymptomTypeID`) REFERENCES `symptom_type` (`SymptomTypeID`),
  CONSTRAINT `user_TO_symptom_code_subset` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `symptom_code_subset` (`SymptomCodeID`, `SymptomTypeID`, `SymptomConditionID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(2, 1, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(3, 1, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(4, 1, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(5, 2, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(6, 2, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(7, 2, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(8, 2, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(9, 2, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(10, 3, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(11, 3, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(12, 3, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(13, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(14, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(15, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(16, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(17, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(18, 4, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(19, 5, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(20, 5, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(21, 5, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(22, 6, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(23, 6, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(24, 6, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(25, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(26, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(27, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(28, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(29, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(30, 7, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(31, 8, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(32, 8, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(33, 9, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(34, 10, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(35, 10, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(36, 11, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(37, 12, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(38, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(39, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(40, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(41, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(42, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(43, 13, NULL, 106, 2, 1, '2013-04-19 13:56:17'),
	(44, 13, NULL, 106, 2, 1, '2013-04-19 14:53:00');


CREATE TABLE IF NOT EXISTS `symptom_condition` (
  `SymptomConditionID` int(11) NOT NULL AUTO_INCREMENT,
  `SymptomConditionName` varchar(50) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SymptomConditionID`),
  KEY `manufacturer_TO_symptom_condition` (`ManufacturerID`),
  KEY `repair_skill_TO_symptom_condition` (`RepairSkillID`),
  KEY `user_TO_symptom_condition` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_symptom_condition` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_symptom_condition` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_symptom_condition` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `symptom_type` (
  `SymptomTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `SymptomTypeName` varchar(50) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SymptomTypeID`),
  KEY `manufacturer_TO_symptom_type` (`ManufacturerID`),
  KEY `repair_skill_TO_symptom_type` (`RepairSkillID`),
  KEY `user_TO_symptom_type` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_symptom_type` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_symptom_type` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_symptom_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `symptom_type` (`SymptomTypeID`, `SymptomTypeName`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'Power', 106, 2, 1, '2013-04-19 11:20:06'),
	(2, 'Battery/Charging/Accessories', 106, 2, 1, '2013-04-19 11:20:06'),
	(3, 'TX/RX/Signal', 106, 2, 1, '2013-04-19 11:20:06'),
	(4, 'Display', 106, 2, 1, '2013-04-19 11:20:06'),
	(5, 'Touch Screen', 106, 2, 1, '2013-04-19 11:20:06'),
	(6, 'Keypad', 106, 2, 1, '2013-04-19 11:20:06'),
	(7, 'Audio', 106, 2, 1, '2013-04-19 11:20:06'),
	(8, 'Cosmetic', 106, 2, 1, '2013-04-19 11:20:06'),
	(9, 'Camera', 106, 2, 1, '2013-04-19 11:20:06'),
	(10, 'Bluetooth', 106, 2, 1, '2013-04-19 11:20:06'),
	(11, 'Infra Red', 106, 2, 1, '2013-04-19 11:20:06'),
	(12, 'FM Radio', 106, 2, 1, '2013-04-19 11:20:06'),
	(13, 'Software/other hardware', 106, 2, 1, '2013-04-19 11:20:06');


CREATE TABLE IF NOT EXISTS `warranty_defect_type` (
  `WarrantyDefectTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `WarrantyDefectTypeCategoryID` int(11) DEFAULT NULL,
  `Code` char(2) NOT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WarrantyDefectTypeID`),
  KEY `user_TO_warranty_service_type` (`ModifiedUserID`),
  KEY `warranty_defect_type_category_TO_warranty_defect_type` (`WarrantyDefectTypeCategoryID`),
  CONSTRAINT `warranty_defect_type_category_TO_warranty_defect_type` FOREIGN KEY (`WarrantyDefectTypeCategoryID`) REFERENCES `warranty_defect_type_category` (`WarrantyDefectTypeCategoryID`),
  CONSTRAINT `user_TO_warranty_defect_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT IGNORE INTO `warranty_defect_type` (`WarrantyDefectTypeID`, `WarrantyDefectTypeCategoryID`, `Code`, `Description`, `Comment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1, 'AE', 'Accessory exchange', 'a faulty accessory has been replaced, no further faults with handset', 1, '2013-04-23 14:10:04'),
	(2, 1, 'HC', 'Handling and Carriage', 'ONLY USED FOR FORWARDING TABLET DEVICES', 1, '2013-04-23 14:10:39'),
	(3, 1, 'IP', 'Inspection Processing', 'No Fault Found - a general inspection of the handset has taken place', 1, '2013-04-23 14:11:05'),
	(4, 1, 'L1', 'Level 1 repair', 'a repair activity with SW only or L1 category parts used', 1, '2013-04-23 14:11:45'),
	(5, 1, 'L2', 'Level 2 repair', 'a repair activity with L2 category parts used', 1, '2013-04-23 14:12:09'),
	(6, 1, 'L3', 'Level 3 repair', 'a repair activity with L3 component levek category parts used', 1, '2013-04-23 14:13:04'),
	(7, 1, 'SU', 'Software Update', 'Customer requests a software upgrade only', 1, '2013-04-23 14:13:00');


CREATE TABLE IF NOT EXISTS `warranty_defect_type_category` (
  `WarrantyDefectTypeCategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) NOT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WarrantyDefectTypeCategoryID`),
  KEY `user_TO_warranty_defect_type_category` (`ModifiedUserID`),
  CONSTRAINT `user_TO_warranty_defect_type_category` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `warranty_defect_type_category` (`WarrantyDefectTypeCategoryID`, `CategoryName`, `Comment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'End User Repairs', NULL, 1, '2013-04-23 14:05:29');


CREATE TABLE IF NOT EXISTS `warranty_defect_type_subset` (
  `WarrantyDefectTypeID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `warranty_service_type_TO_warranty_service_type_subset` (`WarrantyDefectTypeID`),
  KEY `manufacturer_TO_warranty_service_type_subset` (`ManufacturerID`),
  KEY `repair_skill_TO_warranty_service_type_subset` (`RepairSkillID`),
  KEY `user_TO_warranty_service_type_subset` (`ModifiedUserID`),
  CONSTRAINT `warranty_defect_type_TO_warranty_defect_type_subset` FOREIGN KEY (`WarrantyDefectTypeID`) REFERENCES `warranty_defect_type` (`WarrantyDefectTypeID`),
  CONSTRAINT `manufacturer_TO_warranty_defect_type_subset` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_warranty_defect_type_subset` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_warranty_defect_type_subset` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;



CREATE TABLE IF NOT EXISTS `warranty_service_type` (
  `WarrantyServiceTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `WarrantyServiceTypeCategoryID` int(11) DEFAULT NULL,
  `Code` char(2) NOT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WarrantyServiceTypeID`),
  KEY `user_TO_warranty_service_type` (`ModifiedUserID`),
  KEY `warranty_service_type_category_TO_warranty_service_type` (`WarrantyServiceTypeCategoryID`),
  CONSTRAINT `warranty_service_type_category_TO_warranty_service_type` FOREIGN KEY (`WarrantyServiceTypeCategoryID`) REFERENCES `warranty_service_type_category` (`WarrantyServiceTypeCategoryID`),
  CONSTRAINT `user_TO_warranty_service_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `warranty_service_type` (`WarrantyServiceTypeID`, `WarrantyServiceTypeCategoryID`, `Code`, `Description`, `Comment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1, 'PS', 'Pick Up Service', 'ASC initiates a collection of the faulty device from the end customer', 1, '2013-04-23 14:07:51'),
	(2, 1, 'CI', 'Carry In Service', 'end customer carries their faulty device to a walk in service centre', 1, '2013-04-23 14:08:00'),
	(3, 1, 'SH', 'Service Handling', 'To report a No Fault Found claim - see examples sheet', 1, '2013-04-23 14:08:05'),
	(4, 1, 'RH', 'Return Handling', 'Repair cannot be completed due to parts DNA/SNA - see examples sheet', 1, '2013-04-23 14:08:43');


CREATE TABLE IF NOT EXISTS `warranty_service_type_category` (
  `WarrantyServiceTypeCategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) NOT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WarrantyServiceTypeCategoryID`),
  KEY `user_TO_warranty_service_type_category` (`ModifiedUserID`),
  CONSTRAINT `user_TO_warranty_service_type_category` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `warranty_service_type_category` (`WarrantyServiceTypeCategoryID`, `CategoryName`, `Comment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'End User Repairs', NULL, 1, '2013-04-23 14:05:52');


CREATE TABLE IF NOT EXISTS `warranty_service_type_subset` (
  `WarrantyServiceTypeID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `warranty_service_type_TO_warranty_service_type_subset` (`WarrantyServiceTypeID`),
  KEY `manufacturer_TO_warranty_service_type_subset` (`ManufacturerID`),
  KEY `repair_skill_TO_warranty_service_type_subset` (`RepairSkillID`),
  KEY `user_TO_warranty_service_type_subset` (`ModifiedUserID`),
  CONSTRAINT `warranty_service_type_TO_warranty_service_type_subset` FOREIGN KEY (`WarrantyServiceTypeID`) REFERENCES `warranty_service_type` (`WarrantyServiceTypeID`),
  CONSTRAINT `manufacturer_TO_warranty_service_type_subset` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `repair_skill_TO_warranty_service_type_subset` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_warranty_service_type_subset` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `job`
	CHANGE COLUMN `OpenJobStatus` `OpenJobStatus` ENUM('branch_repair','in_store','with_supplier','awaiting_collection','customer_notified','closed') NULL DEFAULT NULL AFTER `StatusID`,
	ADD COLUMN `ServiceCategory` ENUM('soft','physical') NULL DEFAULT NULL AFTER `OpenJobStatus`,
	ADD COLUMN `ConditionCodeID` INT(11) NULL DEFAULT NULL AFTER `ConditionCode`,
	ADD COLUMN `SymptomCodeID` INT(11) NULL DEFAULT NULL AFTER `SymptomCode`,
	ADD COLUMN `WarrantyServiceTypeID` INT(11) NULL DEFAULT NULL AFTER `WarrantyNotes`,
	ADD COLUMN `WarrantyDefectTypeID` INT(11) NULL DEFAULT NULL AFTER `WarrantyServiceTypeID`,
	ADD CONSTRAINT `condition_code_TO_job` FOREIGN KEY (`ConditionCodeID`) REFERENCES `condition_code` (`ConditionCodeID`),
	ADD CONSTRAINT `symptom_code_TO_job` FOREIGN KEY (`SymptomCodeID`) REFERENCES `symptom_code` (`SymptomCodeID`),
	ADD CONSTRAINT `warranty_service_type_TO_job` FOREIGN KEY (`WarrantyServiceTypeID`) REFERENCES `warranty_service_type` (`WarrantyServiceTypeID`),
	ADD CONSTRAINT `warranty_defect_type_TO_job` FOREIGN KEY (`WarrantyDefectTypeID`) REFERENCES `warranty_defect_type` (`WarrantyDefectTypeID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.233');

SET foreign_key_checks = 1;
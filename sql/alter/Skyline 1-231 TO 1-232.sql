# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.231');


# ---------------------------------------------------------------------- #
# Add Table unit_type                                                    #
# ---------------------------------------------------------------------- #
ALTER TABLE unit_type ADD DOPWarrantyPeriod INT( 11 ) NULL AFTER UnitTypeName , ADD ProductionDateWarrantyPeriod INT( 11 ) NULL AFTER DOPWarrantyPeriod ;


# ---------------------------------------------------------------------- #
# Add Permission                                                         #
# ---------------------------------------------------------------------- #
INSERT INTO `permission` (`PermissionID`, `Name`, `Description`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `URLSegment`) VALUES
     ('9000','Data Integrity - Servicebase Referesh','Allow the user to run the Servicebase refresh','2013-04-22 14:23:02','0000-00-00 00:00:00','Active','1','2013-04-22 14:23:16',NULL);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.232');
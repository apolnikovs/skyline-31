/*
 * 	PanelSwitcher 1.0 - jQuery plugin
 *	written by Brian Etherington	
 *	http://www.smokingun.co.uk/index/panelswitcher
 *
 *	Copyright (c) 2011 Brian Etherington (http://www.smokingun.co.uk)
 *	
 *      Dual licensed under the MIT and GPL licenses:
 *      http://www.opensource.org/licenses/mit-license.php
 *      http://www.gnu.org/licenses/gpl.html
 *
 *	Built for jQuery UI library
 *	http://jqueryui.com
 *
 *	Markup example for $('#app-panel').PanelSwitcher();
 *	
 *	<div id="menu">
 *          <ul class="app-panel-Menu"> 
 *              <li class="menuActive">
 *                  <a rel="Home" href="#">Home</a>
 *              </li> 
 *              <li>
 *                  <a rel="Services" href="#">Services</a>
 *              </li>
 *              <li>
 *                  <a rel="Gallery" href="#">Gallery</a>
 *              </li>
 *              <li>
 *                  <a rel="About" href="#">About</a>
 *              </li>
 *              <li>
 *                  <a rel="Contact" href="#">Contact</a>
 *              </li> 
 *          </ul>
 *      </div>
 *	
 *      <div id="app-panel"></div>
 *          
 *      <div id="Home" class="app-panel-Content app-panel-Show">
 *           Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
 *           Integer malesuada varius accumsan
 *      </div>           
 *      <div id="Services" class="app-panel-Content">
 *          Praesent elementum mi at lectus consequat consectetur. 
 *          Pellentesque nec neque euismod turpis imperdiet bibendum. 
 *      </div>          
 *      <div id="Gallery" class="app-panel-Content">
 *          Suspendisse consequat semper mauris et scelerisque. 
 *          Nam pretium odio ut est elementum commodo. 
 *          Proin vehicula cursus sem.
 *      </div> 
 *      <div id="About" class="app-panel-Content">
 *          Donec tristique est a mauris sagittis ut congue quam 
 *          posuere. Aliquam diam lorem, suscipit vel faucibus non, 
 *          congue vel tortor.
 *      </div> 
 *      <div id="Contact" class="app-panel-Content">
 *          Morbi a nulla erat. Curabitur pellentesque purus ut 
 *          mauris eleifend faucibus feugiat sapien pellentesque. 
 *          In hac habitasse platea dictumst.
 *      </div> 
 *
 */

(function( $ ){
    
  $.fn.PanelSwitcher = function( options ) {  

    var settings = {
      'effect'  : 'slide',
      'speed'   : 'slow',
      'dir_in'  : 'right',
      'dir_out' : 'right',
      'OnSwitch' : null
    };

    return this.each(function() { 
        
      // If options exist, lets merge them
      // with our default settings
      if ( options ) { 
        $.extend( settings, options );
      }
      
      var target = $(this);
      var targetId = $(this).attr('id');
      var speed = settings['speed'];
      var effect = settings['effect'];           

      // PanelSwicher plugin code here      
      $('.'+targetId+'-Content').css('display','none');
      $('.'+targetId+'-Show').each(function() { $(target).html($(this).html()); });
      $('.'+targetId+'-Menu').each(function() {
          $(this).find('a').click(function() {
            var _rel = $(this).attr('rel');
            //var _href = $(this).attr('href');
            if (!$(this).parent().hasClass('menuActive')) {
                var $selected = $('#' + _rel);
                //var _content = $selected.html();
                if (speed == 0) {
                    //$(target).html(_content);
                    $(target).html($selected.html());
                    if (settings['OnSwitch'] != null) { settings['OnSwitch'](); }
                } else {
                    switch(effect) {
                        case 'fade':
                            //$(target).fadeOut(speed, function() { $(this).html(_content) } ).fadeIn(speed);
                            $(target).fadeOut(speed, function() { $(this).html($selected.html()) } ).fadeIn(speed, function() { if (settings['OnSwitch'] != null) { settings['OnSwitch'](); } } );
                            break;
                       case 'slide':
                            $(target).hide('slide', {direction: settings['dir_out']}, speed, 
                                            function() { 
                                                $(this).html($selected.html())
                                                .show('slide', {direction: settings['dir_in']}, speed,
                                                        function() { 
                                                            //alert('effect finished');
                                                            //if (settings['OnSwitch'] != null) { settings['OnSwitch'](); }
                                                            myRedraw();
                                                        } );
                                            }  );

                           /*$(target).slideDown(1000, function() { 
                               $(this).html($selected.html())
                               .show(speed, function() { 
                                   if (settings['OnSwitch'] != null) { 
                                       settings['OnSwitch'](); 
                                   }
                               } );
                            }  )*/
                                    
                            break;
                        default:
                            //$(target).html(_content);
                            $(target).html($selected.html());
                            if (settings['OnSwitch'] != null) { settings['OnSwitch'](); }
                            break;
                    }
                }
                
                $('.'+targetId+'-Menu .menuActive').each( function() { $(this).removeClass('menuActive'); } );
                $('[rel='+_rel+']').each( function() {  $(this).parent().addClass('menuActive'); } );
                  
            }

          }); // click function
      
      }); // each menu...
      
    }); // main function...
    
  }; // PanelSwitcher

})( jQuery );



<?php

/**
 * PostcodeLatLong
 * 
 * This model handles conversion of postcodes via latitude and longtude using the
 * postcode_lat_long table.
 *
 * @author      Andrew Williams <Andrew.Williams@awcomputech.com>
 * 
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 12/10/2012  1.00    Andrew J. Williams   Initial Version   
 * 30/10/2012  1.01    Andrew J. Williams   Error handling for lookup added
 * 31/10/2012  1.02    Andrew J. Williams   Deal with missing psace in post codes           
 * 10/12/2012  1.03    Andrew J. Williams   Issue 151 - Service Provider Geo Tags
 * 04/02/2012  1.04    Andrew J. Williams   Added missing require for Table Factory Class
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class PostcodeLatLong extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    private $conn;
    public $debug = false;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->table = TableFactory::PostcodeLatLong();
    }

    /**
     * getLastStatus
     *  
     * Get the last status of a job
     * 
     * @param array $pc     Postcode to find
     * 
     * @return string       Array containing laitude and logitude
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getLatLong($pc) {
        $pc = strtoupper($pc);
        $pc = preg_replace('/[^A-Z0-9]/', '', $pc);
        $pc = preg_replace('/([A-Z0-9]{3})$/', ' \1', $pc);
        $pc = trim($pc);
        
        $retval = array();                                                      /* Array to hold return value */
        
        /***************************
        * Geo Tag service Provider *
        ***************************/
         // $this->controller->log('ee','geotag_log_');              
        if (isset($this->controller->user->ServiceProviderID)) {                      /* Check if service provider is set - if it is we can use service provider geo tag file*/                  
            $sql_geotag = "
                            SELECT
                                    `Latitude`,
                                    `Longitude`
                            FROM
                                    `service_provider_geotags`
                            WHERE
                                    `Postcode` = '$pc'
                                    AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID}
                          ";
         // $this->controller->log($sql_geotag,'geotag_log_');                          
            $result = $this->Query($this->conn, $sql_geotag);

            if ( count($result) > 0 ) {                                         /* Record found */
                $retval['latitude'] = (float) $result[0]['Latitude'];           /* Latitude */
                $retval['longitude'] = (float) $result[0]['Longitude'];         /* Longitude */
                
                return($retval);                                                /* Since we have it exit and return */
            } /* count $result > 0 */
            /* If here not found so carry on and look up in postcode_lat_long table */
        } /* isset $this->controller->ServiceProviderID */
        
        /*************************
        * Postcode Lat Long File * 
        *************************/
        
        $sql = "
                SELECT
			`Latitude`,
			`Longitude`
		FROM
			`postcode_lat_long`
		WHERE
			`PostCode` = '$pc'
                ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {                                             /* Record found */
            $retval['latitude'] = (float) $result[0]['Latitude'];               /* Latitude */
            $retval['longitude'] = (float) $result[0]['Longitude'];             /* Longitude */
        } else {
            $lu = $this->WebLookUp($pc);                                        /* Not found - look up on the web */
            
            if (isset($lu['error'])) {
                $retval['latitude'] = -1;                                       /* Can't find so set return vals to -1 */
                $retval['longitude'] = -1;
            } else {
                $retval['latitude'] = $lu['latitude'];                          /* Found so set return values to latitude and longitude */
                $retval['longitude'] = $lu['longitude'];
                
                $args = array(                                                  /* Write to database */
                              'PostCode' => $pc,
                              'Latitude' => $lu['latitude'],
                              'Latitude' => $lu['longitude']
                             );
                
                $cmd = $this->table->updateCommand( $args );
        
                $this->Execute($this->conn, $cmd, $args);
            }
        }
        return($retval);
    }
   
    /**
     * WebLookup
     *  
     * Get the latitude and logitude of a postcode from the web
     * 
     * @param $postcode The Postcode
     * 
     * @return Array containing laitude and logitude
     * 
     * @author http://www.uk-postcodes.com/api.php
     * @author https://gist.github.com/364477 
     **************************************************************************/
    
    public function WebLookUp($postcode) {
        $retval = array();
        $postcode = str_replace(" ", "", $postcode);
        if($postcode)
        {    
            $url = "http://www.uk-postcodes.com/postcode/". urlencode($postcode) .".csv"; // Build the URL
            
            if($this->debug) $this->controller->log("WebLookUp::PostcodeLatLong - Calling URL $url",'postcodelatlong_');

            if (file_exists ($url) ) {
                $file = file_get_contents($url);
            } else {
                $retval['error'] = TRUE;
                if($this->debug) $this->controller->log("WebLookUp::PostcodeLatLong - $url does not exist",'postcodelatlong_');
                $this->controller->log("WebLookUp::PostcodeLatLong - Cannot lookup postcode at $url");
                return ($retval);
            }
            
            //$file = file_get_contents($url);
            
            if($this->debug) $this->controller->log("WebLookUp::PostcodeLatLong - File returned $file",'postcodelatlong_');

            $retVal = array();

            if (strpos($file, "html") === FALSE) { // Some error checking - if the file contains html, then we've been redirected to the homepage and something has gone wrong
                $pieces = explode(",", $file);
                //$retval['postcode'] = $pieces[0];
                $retval['latitude'] = (float) $pieces[1];
                $retval['longitude'] = (float) $pieces[2];
            } else {
                $retval['error'] = TRUE; // If an error, return one
            }
        } 
        else
        {
            $retval['error'] = TRUE;
        }
         if($this->debug) $this->controller->log("WebLookUp::PostcodeLatLong - Exiting return value =  ".var_export($retVal,true),'postcodelatlong_');
        
        return ($retval);
    }
}

?>

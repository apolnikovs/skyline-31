<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Group Headings Page in Lookup Tables section under System Admin
 *
 * @author     Thirumalesh Bandi <t.bandi@pccsuk.com>
 * @version     1.0
 */

class GroupHeadings extends CustomModel {
    
    private $conn;
    private $dbColumns = array('GroupID', 'GroupHeading', 'Description', 'Status');
    private $table     = "group_headings";
    public $page;
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com>
     */  
    public function fetch($args) {
        
        
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['GroupID']) || !$args['GroupID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT GroupID,GroupHeading, Description, Status, EndDate FROM '.$this->table.' WHERE GroupID=:GroupID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':GroupID' => $args['GroupID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
        
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com>
     */ 
    public function update($args) {
        
        if($args['GroupID'])
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            
            if($args['Status']=='In-active')
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }
            else
            {
                $EndDate = $row_data['EndDate'];
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              GroupHeading=:GroupHeading, Description=:Description, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate

              WHERE GroupID=:GroupID';
       
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':GroupHeading' => $args['GroupHeading'], 
                        ':Description' => $args['Description'],   
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':GroupID' => $args['GroupID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetching the menu item list from database,which are activated.    
     * @param $menuID is a ParentID
     * @global $this->table
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com>
     */
    
    public function fetchMenuList($menuID)
    {
     
       $sql = 'SELECT 
                        GroupID AS GID,
                        GroupHeading AS title, 
                        Code AS code, 
                        URLSegment AS link, 
                        Tooltip AS tooltip, 
                        Description AS description, 
                        Description AS shortDesc
                       
                        FROM '.$this->table.' WHERE ParentID='.$menuID.' AND Status="Active"';
       
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));        
        
        $fetchQuery->execute();
        $result = $fetchQuery->fetchAll();
        
        return $result;   
        
    }
   
    
         /**
     * Description
     * 
     * This method is used for to fetching the page title and description from database.    
     * @param $Code is a group heading
     * @global $this->table
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com>
     */
    
    
    public function getHeadingPageDetails($Code)
    {
        
         $sql = 'SELECT 
                        GroupHeading,                        
                        Description                       
                        FROM '.$this->table.' WHERE Code=:Code ';
               
               
       
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));        
        
        $fetchQuery-> execute(array(':Code' => $Code));
        $result = $fetchQuery->fetch();
        
        return $result;
        
    }
   
}
?>
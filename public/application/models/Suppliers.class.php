<?php

require_once('CustomModel.class.php');


/**
 * Suppliers.class.php
 *
 * This class is used for handling database actions for Suppliers
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @copyright   2013 PC Control Systems
 * @link       
 * @version     1.01
 * 
 * Changes
 * Date        Version Author                Reason
 * 27/03/2013  1.00    Andris Polnikovs      Initial Version
 * 11/06/2013  1.01    Andris Polnikovs      CopyFromMain functionality
 ******************************************************************************/

class Suppliers extends CustomModel {
    
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       
   
    $this->debug = false;
   //$this->debug=true;
    $this->comonFields =[
         "CompanyName",
        "AccountNumber",
        "PostCode",
        "BuildingNameNumber",
        "Street",
        "LocalArea",
        "TownCity",
        "CountryID",
        "TelephoneNo",
        "FaxNo",
        "EmailAddress",
        "Website", 
    "ContactName", 
    "DirectOrderTemplateID", 
    "DefaultPostageCharge", 
    "DefaultCurrencyID", 
    "NormalSupplyPeriod", 
    "UsageHistoryPeriod", 
    "MinimumOrderValue", 
    "MultiplyByFactor", 
    "OrderPermittedVariancePercent",
    "CollectSupplierOrderNo",
    "PostageChargePrompt",
    "TaxExempt",
    "Status",
    
    ];
    }
   
    

   /********************************************************************************
    * Description
    * 
    * This method is for fetching all supliers for future use in datatables
    * 
    * @param 
    * @return
    * 
    * @author Andris Polnikovs <a.polnikovs@gmail.com>******************************
    */
    public function getAllData($table)
    {
       
        $sql="select * from $table where Status='Active'";
     $res=$this->Query($this->conn, $sql);
     return $res;
        
        
    }

    
     public function saveSupplier($p,$table,$secondaryTable=false){
         $extraCol="";
         $extraToken="";
         $extraParam=array();
          if(!isset($p['Status'])){$p['Status']="Active";}
          if(!isset($p['CollectSupplierOrderNo'])){$p['CollectSupplierOrderNo']="No";}
         if(!isset($p['PostageChargePrompt'])){$p['PostageChargePrompt']="No";}
         if(!isset($p['TaxExempt'])){$p['TaxExempt']="No";}
         
     ($table=="supplier")?$idcol="SupplierID":$idcol="ServiceProviderSupplierID";
     
     
     
         if($p['mode']=="insert" ||$p['mode']=="copyNew"){
             
             
             if($p['mode']=="copyNew"){
                 $extraCol=",ServiceProviderID,SupplierID";
                 $extraToken=",:ServiceProviderID,:SupplierID";
                 $extraParam= $params=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "SupplierID" =>$p["id"], 
                     );
             }
             
         $sql="insert into $table 
        (
        CompanyName,
        AccountNumber,
        ShipToAccountNumber,
        PostCode,
        BuildingNameNumber,
        Street,
        LocalArea,
        TownCity,
        CountryID,
        TelephoneNo,
        FaxNo,
        EmailAddress,
        Website, 
    ContactName, 
    DirectOrderTemplateID, 
    DefaultPostageCharge, 
    DefaultCurrencyID, 
    NormalSupplyPeriod, 
    UsageHistoryPeriod, 
    MinimumOrderValue, 
    MultiplyByFactor, 
    OrderPermittedVariancePercent,
    CollectSupplierOrderNo,
    PostageChargePrompt,
    TaxExempt,
    Status,
    ModifiedUserID
    $extraCol
        
        ) 
        values
         (
        :CompanyName,
        :AccountNumber,
        :ShipToAccountNumber,
        :PostCode,
        :BuildingNameNumber,
        :Street,
        :LocalArea,
        :TownCity,
        :CountryID,
        :TelephoneNo,
        :FaxNo,
        :EmailAddress,
        :Website, 
    :ContactName, 
    :DirectOrderTemplateID, 
    :DefaultPostageCharge, 
    :DefaultCurrencyID, 
    :NormalSupplyPeriod, 
    :UsageHistoryPeriod, 
    :MinimumOrderValue, 
    :MultiplyByFactor, 
    :OrderPermittedVariancePercent,
    :CollectSupplierOrderNo,
    :PostageChargePrompt,
    :TaxExempt,
    :Status,
    :ModifiedUserID
    $extraToken
        ) 
";
         
         
         }
        //this is used when new entry is created it must be insertedfirst in main table, then secondary
         if($p['mode']=="New"){
            
              $extraCol=",ApproveStatus";
                 $extraToken=",:ApproveStatus";
                 $extraParam= $params=array(
     "ApproveStatus" =>"Pending", 
     
                     );
               $sql="insert into $secondaryTable 
        (
        CompanyName,
        AccountNumber,
        ShipToAccountNumber,
        PostCode,
        BuildingNameNumber,
        Street,
        LocalArea,
        TownCity,
        CountryID,
        TelephoneNo,
        FaxNo,
        EmailAddress,
        Website, 
    ContactName, 
    DirectOrderTemplateID, 
    DefaultPostageCharge, 
    DefaultCurrencyID, 
    NormalSupplyPeriod, 
    UsageHistoryPeriod, 
    MinimumOrderValue, 
    MultiplyByFactor, 
    OrderPermittedVariancePercent,
    CollectSupplierOrderNo,
    PostageChargePrompt,
    TaxExempt,
    Status,
    ModifiedUserID
   
    $extraCol
        
        ) 
        values
         (
        :CompanyName,
        :AccountNumber,
        :ShipToAccountNumber,
        :PostCode,
        :BuildingNameNumber,
        :Street,
        :LocalArea,
        :TownCity,
        :CountryID,
        :TelephoneNo,
        :FaxNo,
        :EmailAddress,
        :Website, 
    :ContactName, 
    :DirectOrderTemplateID, 
    :DefaultPostageCharge, 
    :DefaultCurrencyID, 
    :NormalSupplyPeriod, 
    :UsageHistoryPeriod, 
    :MinimumOrderValue, 
    :MultiplyByFactor, 
    :OrderPermittedVariancePercent,
    :CollectSupplierOrderNo,
    :PostageChargePrompt,
    :TaxExempt,
    :Status,
    :ModifiedUserID
   
             $extraToken
        ) 
"; 
               
               
               
       $params=array(
     "CompanyName" =>$p["CompanyName"], 
    "AccountNumber" =>$p["AccountNumber"],
    "ShipToAccountNumber" =>$p["ShipToAccountNumber"],
    "PostCode" =>$p["PostCode"], 
    "BuildingNameNumber" =>$p["BuildingNameNumber"], 
    "Street" =>$p["Street"], 
    "LocalArea" =>$p["LocalArea"], 
    "TownCity" =>$p["TownCity"], 
    "CountryID" =>$p["CountryID"], 
    "TelephoneNo" =>$p["TelephoneNo"], 
    "FaxNo" =>$p["FaxNo"], 
    "EmailAddress" =>$p["EmailAddress"], 
    "Website" =>$p["Website"], 
    "ContactName" =>$p["ContactName"], 
    "DirectOrderTemplateID" =>$p["DirectOrderTemplateID"], 
    "DefaultPostageCharge" =>$p["DefaultPostageCharge"], 
    "DefaultCurrencyID" =>$p["DefaultCurrencyID"], 
    "NormalSupplyPeriod" =>$p["NormalSupplyPeriod"], 
    "UsageHistoryPeriod" =>$p["UsageHistoryPeriod"], 
    "MinimumOrderValue" =>$p["MinimumOrderValue"], 
    "MultiplyByFactor" =>$p["MultiplyByFactor"], 
    "OrderPermittedVariancePercent" =>$p["OrderPermittedVariancePercent"],
    "CollectSupplierOrderNo"=>$p['CollectSupplierOrderNo'],
    "PostageChargePrompt"=>$p['PostageChargePrompt'],
    "TaxExempt"=>$p['TaxExempt'],
    "Status"=>$p['Status'],
    "ModifiedUserID"=>$this->controller->user->UserID,
         );
    $params1=  array_merge($params,$extraParam);
             $this->Execute($this->conn, $sql,$params1);  
           $mainID=$this->conn->lastInsertId();   
            $extraCol=",ServiceProviderID,SupplierID";
                 $extraToken=",:ServiceProviderID,:SupplierID";
                 $extraParam=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "SupplierID" =>$mainID, 
                     );
              $sql="insert into $table
        (
        CompanyName,
        AccountNumber,
        ShipToAccountNumber,
        PostCode,
        BuildingNameNumber,
        Street,
        LocalArea,
        TownCity,
        CountryID,
        TelephoneNo,
        FaxNo,
        EmailAddress,
        Website, 
    ContactName, 
    DirectOrderTemplateID, 
    DefaultPostageCharge, 
    DefaultCurrencyID, 
    NormalSupplyPeriod, 
    UsageHistoryPeriod, 
    MinimumOrderValue, 
    MultiplyByFactor, 
    OrderPermittedVariancePercent,
    CollectSupplierOrderNo,
    PostageChargePrompt,
    TaxExempt,
    Status,
    ModifiedUserID
    $extraCol
        
        ) 
        values
         (
        :CompanyName,
        :AccountNumber,
        :ShipToAccountNumber,
        :PostCode,
        :BuildingNameNumber,
        :Street,
        :LocalArea,
        :TownCity,
        :CountryID,
        :TelephoneNo,
        :FaxNo,
        :EmailAddress,
        :Website, 
    :ContactName, 
    :DirectOrderTemplateID, 
    :DefaultPostageCharge, 
    :DefaultCurrencyID, 
    :NormalSupplyPeriod, 
    :UsageHistoryPeriod, 
    :MinimumOrderValue, 
    :MultiplyByFactor, 
    :OrderPermittedVariancePercent,
    :CollectSupplierOrderNo,
    :PostageChargePrompt,
    :TaxExempt,
    :Status,
    :ModifiedUserID
                 $extraToken
        ) 
"; 
              
              
            
              $params=  array_merge($params,$extraParam);
         
           $this->Execute($this->conn, $sql,$params);       
              
         }
        
          
             
             
         
      if($p['mode']=="update"){
    
          $id=$p['id'];
          if(isset($p['ApproveStatus'])){
              $st=$p['ApproveStatus'];
              $ApproveStatus=",ApproveStatus='$st'";
          }else{
              $ApproveStatus="";
          }
          $sql="
             update $table set
             
              CompanyName=:CompanyName,
        AccountNumber=:AccountNumber,
        ShipToAccountNumber=:ShipToAccountNumber,
        PostCode=:PostCode,
        BuildingNameNumber=:BuildingNameNumber,
        Street=:Street,
        LocalArea=:LocalArea,
        TownCity=:TownCity,
        CountryID=:CountryID,
        TelephoneNo=:TelephoneNo,
        FaxNo=:FaxNo,
        EmailAddress=:EmailAddress,
        Website=:Website, 
    ContactName=:ContactName, 
    DirectOrderTemplateID=:DirectOrderTemplateID, 
    DefaultPostageCharge=:DefaultPostageCharge, 
    DefaultCurrencyID=:DefaultCurrencyID, 
    NormalSupplyPeriod=:NormalSupplyPeriod, 
    UsageHistoryPeriod=:UsageHistoryPeriod, 
    MinimumOrderValue=:MinimumOrderValue, 
    MultiplyByFactor=:MultiplyByFactor, 
    OrderPermittedVariancePercent=:OrderPermittedVariancePercent,
    CollectSupplierOrderNo=:CollectSupplierOrderNo,
    PostageChargePrompt=:PostageChargePrompt,
    TaxExempt=:TaxExempt,
    Status=:Status,
    ModifiedUserID=:ModifiedUserID
    $ApproveStatus
        where $idcol=$id

            ";//CreatedDate=:CreatedDate,
      }
      /*if($p['CreatedDate']=="0000-00-00 00:00:00"){
             $p['CreatedDate']=date('Y-m-d H:i:s');
          }*/
       $params=array(
     "CompanyName" =>$p["CompanyName"], 
    "AccountNumber" =>$p["AccountNumber"], 
    "ShipToAccountNumber"=>$p["ShipToAccountNumber"],
    "PostCode" =>$p["PostCode"], 
    "BuildingNameNumber" =>$p["BuildingNameNumber"], 
    "Street" =>$p["Street"], 
    "LocalArea" =>$p["LocalArea"], 
    "TownCity" =>$p["TownCity"], 
    "CountryID" =>$p["CountryID"], 
    "TelephoneNo" =>$p["TelephoneNo"], 
    "FaxNo" =>$p["FaxNo"], 
    "EmailAddress" =>$p["EmailAddress"], 
    "Website" =>$p["Website"], 
    "ContactName" =>$p["ContactName"], 
    "DirectOrderTemplateID" =>$p["DirectOrderTemplateID"], 
    "DefaultPostageCharge" =>$p["DefaultPostageCharge"], 
    "DefaultCurrencyID" =>$p["DefaultCurrencyID"], 
    "NormalSupplyPeriod" =>$p["NormalSupplyPeriod"], 
    "UsageHistoryPeriod" =>$p["UsageHistoryPeriod"], 
    "MinimumOrderValue" =>$p["MinimumOrderValue"], 
    "MultiplyByFactor" =>$p["MultiplyByFactor"], 
    "OrderPermittedVariancePercent" =>$p["OrderPermittedVariancePercent"],
    "CollectSupplierOrderNo"=>$p['CollectSupplierOrderNo'],
    "PostageChargePrompt"=>$p['PostageChargePrompt'],
    "TaxExempt"=>$p['TaxExempt'],
    "Status"=>$p['Status'],
    //"CreatedDate"=>$p['CreatedDate'],
   "ModifiedUserID"=>$this->controller->user->UserID,
         );
      if($p['mode']!="New"){
          $params=  array_merge($params,$extraParam);
          $this->Execute($this->conn, $sql,$params);  
      }
     }
     
     
     
     
   public function getData($id,$table){
       ($table=="supplier")?$idcol="SupplierID":$idcol="ServiceProviderSupplierID";
       $sql="select * from $table where $idcol=$id";
       $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
   
   public function deleteSupplier($id,$table){
        ($table=="supplier")?$idcol="SupplierID":$idcol="ServiceProviderSupplierID";
       $sql="update $table set Status='In-active' where $idcol=$id";
       $res=$this->Execute($this->conn, $sql);
   }
   
    public function getAllSuppliersData($limit='')
    {
        if($limit!=''){
            $limit="limit $limit";
        }
        $sql="select * from supplier where Status='Active' $limit";
     $res=$this->Query($this->conn, $sql);
     return $res;
        
        
    }
    
    
  public function getIDFromMain($spid){
      $sql="select ServiceProviderID from service_provider_supplier ss where ss.SupplierID=$spid";
      $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0]['ServiceProviderID'];
     }else{
         false;
     }
  }  
  
  public function getSPSuppliers($spID){
      if($spID==""){
          $sql="select * from service_provider_supplier where Status='Active'";
      }else{
      $sql="select * from service_provider_supplier where Status='Active' and ServiceProviderID=$spID";
      }
      $res=$this->Query($this->conn, $sql);
    
     return $res;
  }
  
  public function CopyFromMain($ids, $spid) {
        $fields = $this->comonFields;
        $sql = "insert into service_provider_supplier (";
        foreach ($fields as $c) {
            $sql.="$c,";
        }
        $sql.="ServiceProviderID,SupplierID,CreatedDate";

        $sql = trim($sql, ",");
        $sql.=") select ";
        foreach ($fields as $c) {
            $sql.="$c,";
        }
        $sql = trim($sql, ",");
        //extra paramtres
        $sql.=" ,'$spid' as ServiceProviderID,SupplierID,''
            
        ";
        $sql.=" from supplier mmm where SupplierID in (";
        foreach ($ids as $c) {
            $sql.="$c,";
        }
        $sql = trim($sql, ",");
        $sql.=")";
        
        $this->Execute($this->conn, $sql);
    }

    public function getMainTableDistinctRecords($spid) {
        $sql = "select count(*) as c from supplier where SupplierID not in (select SupplierID from service_provider_supplier where ServiceProviderID=$spid) and ApproveStatus='Approved' and Status='Active'";
        $result = $this->query($this->conn, $sql);
        return $result[0]["c"];
    }

    //this function counts how many records have been copied from supplier table, but have not been modified by user yet
    public function countSPUnmodifiedRecords($spID) {
        $sql = "select count(*) as con from service_provider_supplier spm where CreatedDate='0000-00-00 00:00:00'";
        $result = $this->query($this->conn, $sql);
        return $result[0]['con'];
    }
    
}



?>
  

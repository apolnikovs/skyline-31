<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script> 

<script type="text/javascript">   
     
 $(document).ready(function() {


var t=$('#ServiceProviderSupplierID').combobox({

change:function(){
$('#supplierError').remove();
}
});
$('#ServiceProviderSupplierID').next('span').children('input').attr('tabindex',2);
console.log();
$('#ServiceProviderColourID').combobox();
$('#ServiceProviderColourID').next('span').children('input').attr('tabindex',7);
//$('#PrimaryServiceProviderModelID').combobox();
//$('#PrimaryServiceProviderModelID').next('span').children('input').attr('tabindex',8);
var stock=[
{foreach $stockTemplates as $d}

"{$d.PartNumber}",
{/foreach}
];
$('#PartNumber').autocomplete({
      source: stock,
      change: function() {
      $('#PartNumberError').remove();
          if(jQuery.inArray($(this).val(), stock)>-1){
          $.post("{$_subdomain}/Stock/getSpPartTemplateData", { "PartNumber":$('#PartNumber').val()},
		    function(data) { 
                   en=jQuery.parseJSON(data);
                   //console.log(en);
                   $('#Description').val(en.Description);
                   $('#ServiceProviderColourID').val(en.ServiceProviderColourID); 
                   $('#PrimaryServiceProviderModelID').val(en.PrimaryPrimaryServiceProviderModelID); 
                   $("#ServiceProviderColourID").next(".ui-combobox").children('.ui-autocomplete-input').val($("#ServiceProviderColourID option:selected").text());;
                   $('#ServiceProviderSupplierID').val(en.DefaultServiceProviderSupplierID).next(".ui-combobox").children('.ui-autocomplete-input').val($("#ServiceProviderSupplierID option:selected").text());
                   $('#MinStock').val(en.MinStock);
                   $('#MakeUpTo').val(en.MakeUpTo);
                    });
        }
      }
     
    });
   var oTable = $('#Results').dataTable( {
"sDom": 'f<bottom>pi',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		 "oLanguage": {
                                
                                "sSearch": "{$page['Labels']['FilterResultsBy']}:"
                            },
    "sAjaxSource": "{$_subdomain}/Stock/loadStockReceivingHistoryList/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			//aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                               
                                $.colorbox.resize();
                            
                               
			} );
                        },
                        
                            
                        

"iDisplayLength" : 10,
"bPaginate":true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 ], [25, 50, 100]],
"aoColumns": [ 
			
			
			
                               
                           
                           
			 
                      
                                 { "bVisible":0 },
                               true,
                               true,
                               true,
                               true
                               
                            
		],
                "bFilter": true,
                 "aaSorting": [ [1,'desc'] ]
               
   
 
        
          
});//datatable end

/* Add a click handler to the rows - this could be used as a callback */
	$("#Results tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}


$('#ReceivingFinishButton').click( function() {
                 oTable=oTable = $('#Results').dataTable();
		 oTable.fnReloadAjax("{$_subdomain}/Stock/loadStockReceivingHistoryList/");
               
	} );     



/* Add a click handler for the edit row */
	$('button[id^=OrderHistoryButton]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
       $('#view1').hide();
$('#view2').hide();
$('#view3').show();
//console.log(anSelected[0]);
//console.log(aData[0]);
 $.post("{$_subdomain}/Stock/getOrderHistoryData", { "id":aData[0]},
		    function(data) { 
                   en=jQuery.parseJSON(data);
                   //console.log(en);
                   $('#orderHistoryNumber').html(en[0].OrderNo);
                   $('#orderHistorySupplier').html(en[0].CompanyName);
                   $('#orderHistoryReceivedBy').html(en[0].ReceivedBy);
                   $('#orderHistoryReceivedDate').html(en[0].ReceivedDate);
                   
                   
  var oTable2 = $('#stockHistoryTable').dataTable( {
                "sDom": '<bottom>',

                 "bServerSide": true,

		 "oLanguage": {
                                
                                "sSearch": "Filter Results By:"
                            },
                "sAjaxSource": "{$_subdomain}/Stock/loadOrderReceivingHistoryList/id="+aData[0]+"/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			//aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                               
                                $.colorbox.resize();
                            
                               
			} );
                        },
                        
                            
                        

                "iDisplayLength" : -1,
                "bPaginate":false,
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[ 25, 50, 100 ], [25, 50, 100]],
                "aoColumns": [ 
			
                               true,
                               true,
                               true
                            
		],
                "bFilter": true,
                 "aaSorting": [ [1,'desc'] ]
               
   
 
        
          
});//datatable end
                   
                   
                   
                   
                   });
$.colorbox.resize();
    }else{
    alert("Please select row first");
    }
		
	} );          
   }); //doc ready end
   
   

function receiveStockToggle(){
    
$('#view1').toggle();
$('#view2').toggle();
$.colorbox.resize();
}


function receiveStockHistoryFinish(){
    
$('#view1').show();
$('#view2').hide();
$('#view3').hide();
oTable2 = $('#stockHistoryTable').dataTable();
oTable2.fnDestroy();
$.colorbox.resize();
}

function insertStockPart(d){
        
        
        $('#NextStockButton').hide();
        $('#loadergif').show();
        
        $("#OrderNo").valid();
        if($("#ServiceProviderSupplierID").val()==""){
        $('#supplierError').remove();
        $("#ServiceProviderSupplierID").parent('td').append("<label class='error' generated='true' id='supplierError'>This field is required</label>");
        };
         if($("#PartNumber").val()==""){
          $('#PartNumberError').remove();
        $("#PartNumber").parent('td').append("<label class='error' generated='true' id='PartNumberError'>This field is required</label>");
        }
        $("#qty").valid();
        $("#PurchaseCost").valid();
        $("#Description").valid();
        
        
        $.colorbox.resize();
        //console.log($("#qty").val());
        if($("#ServiceProviderSupplierID").val()!=""&&$("#OrderNo").val()!=""&&$("#PartNumber").val()!=""&&$("#qty").val()!=""&&$("#PurchaseCost").val()!=""&&$("#Description").val()!=""){
        
            //insert stock item
            
            $.post("{$_subdomain}/Stock/insertStockItem/",  $('#receiveStockForm').serialize(),
		    function(data) { 
                     oTable=oTable = $('#Results').dataTable();
		 oTable.fnReloadAjax("{$_subdomain}/Stock/loadStockReceivingHistoryList/");
                     //console.log("post1");
                    $('#NextStockButton').show();
        $('#loadergif').hide();
         $('#ServiceProviderSupplierID').val('').next(".ui-combobox").children('.ui-autocomplete-input').val($("#ServiceProviderSupplierID option:selected").text());
                   $("#OrderNo").val('');
                   $('#Description').val('');
                   $('#ServiceProviderColourID').val(''); 
                   $('#PrimaryServiceProviderModelID').val(''); 
                   $("#ServiceProviderColourID").next(".ui-combobox").children('.ui-autocomplete-input').val('');;
                   //$('#PrimaryServiceProviderModelID').val(en.PrimaryServiceProviderModelID).next(".ui-combobox").children('.ui-autocomplete-input').val('');
                   $('#MinStock').val('');
                   $('#qty').val('');
                   $('#PurchaseCost').val('');
                   $('#PartNumber').val('').next('.ui-autocomplete-input').val('');
                   $('#MakeUpTo').val('');
                   showAdded();
                   if(d==1){
                   receiveStockToggle();
                   }
                    }
                    );
        
        
        
        }else{
        $('#NextStockButton').show();
        $('#loadergif').hide();
        }
	};


function checkOrderNo()
{
    $('#WarningMsg').hide();
$.post("{$_subdomain}/Stock/checkOrderNo", { "orderNo":$('#OrderNo').val()},
		    function(data) { 
               en=jQuery.parseJSON(data);
               console.log(en);
            if(en!=null){ //console.log("ff");
            if(en['ReceivedByUserID']!=null){
            $('#onumDisplay').html($('#OrderNo').val());
            $('#WarningMsg').show();
            $.colorbox.resize();
            }else{
            //order number is found and it got ordered date then populating all fields except qty as it may differ in some cases
             $('#Description').val(en.Description);
                   $('#ServiceProviderColourID').val(en.ServiceProviderColourID); 
                   $('#PrimaryServiceProviderModelID').val(en.PrimaryPrimaryServiceProviderModelID); 
                   $("#ServiceProviderColourID").next(".ui-combobox").children('.ui-autocomplete-input').val($("#ServiceProviderColourID option:selected").text());;
                   $('#ServiceProviderSupplierID').val(en.DefaultServiceProviderSupplierID).next(".ui-combobox").children('.ui-autocomplete-input').val($("#ServiceProviderSupplierID option:selected").text());
                   $('#MinStock').val(en.MinStock);
                   $('#MakeUpTo').val(en.MakeUpTo);
                   $('#PartNumber').val(en.PartNumber);
                   $('#PurchaseCost').val(en.PurchaseCost);
                   $('#qty').val(en.Quantity);
                   
            }
            }else{
            $('#WarningMsg').hide();
            $.colorbox.resize();
            }
 

});
}


function showAdded(){
$('#AddetMsg').show().delay(5000).fadeOut();;
}

function stockReceivingTableRefresh(){

}
  </script> 
  
  <style>
    .ui-combobox input{
        width:200px!important;
        }
       #formTable  td{
             background:none!important;
             text-align:left!important;
            }
     #formTable td input{
        width:200px!important;;
       
        }
        #formTable td label{
        width:100px!important;;
        float:left;
        }
        sup{
            color:red;
            }
        .error{
         width:200px!important;   
            }
            .ui-combobox-input{
                text-transform:uppercase;
                }
</style> 
   
<div id="StockHistoryPopupContainer" class="SystemAdminFormPanel" style="width:900px;">
    <div id="view1">
    <fieldset>
        <legend>{$page['Text']['title_text']}</legend>
    
        
         <div id="shButtons">
             <button id="OrderHistoryButton" type="button" style="float: left" class="gplus-blue">{$page['Buttons']['OrderHistory']}</button>
             <button onclick="receiveStockToggle();" id="ReceiveStockButton2" type="button" style="float: left" class="gplus-blue">{$page['Buttons']['ReceiveStock']}</button>
           
        </div>
     <table id="Results"  border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                <th>{$page['Labels']['id']}</th>
                                <th>{$page['Labels']['date']}</th>
                                <th>{$page['Labels']['OrderNo']}</th>
                                <th>{$page['Labels']['Supplier']}</th>
                                <th>{$page['Labels']['ReceivedBy']}</th>
			    </tr>
                        </thead>
                        <tbody>
                          
                          
                        </tbody>
                    </table>  
        
        
        <div id="shButtons">
             <button id="OrderHistoryButton" type="button" style="float: left" class="gplus-blue">{$page['Buttons']['OrderHistory']}</button>
             <button onclick="receiveStockToggle();" id="ReceiveStockButton" type="button" style="float: left" class="gplus-blue">{$page['Buttons']['ReceiveStock']}</button>
             <button  onclick="$.colorbox.close();" id="OrderHistoryCloseButton" type="button" style="float: right" class="gplus-blue">{$page['Buttons']['Finish']}</button>
        </div>
    
    </fieldset>    
                 
                        
    </div>
    <div id="view2" style="display:none;">
    <fieldset>
        <legend>{$page['Labels']['ReceiveStock']}</legend>
         <div id="WarningMsg" style="text-align: center;background:#FFE8F1;border:1px solid red;padding: 4px;margin-top: 10px;display:none;">
                           
              {$page['Errors']['AlertOrderNumber']} <span id="onumDisplay" style="font-weight:bold"></span> {$page['Errors']['PreviouslyEntered']}
         </div>
         <div id="AddetMsg" style="text-align: center;background:#caf4c1;border:1px solid green;padding: 4px;margin-top: 10px;display:none;">
                           
            {$page['Errors']['ItemAddettoStock']} 
         </div>
        <form id="receiveStockForm" name="receiveStockForm" action="#" method="post">
        <table id="formTable">
            <tr>
                <td style="width:120px;">
                    <label class="cardLabel" for="OrderNo">{$page['Labels']['OrderNo']}<sup>*</sup>:</label>
		</td>
               <td style="width:120px;">
                   <input tabindex=1  onchange="checkOrderNo()"  validate required="required" class="text required uppercaseText" type="text" id="OrderNo" name="OrderNo" value="" />
                </td>
               
                <td>
                      <label class="cardLabel" for="Description">{$page['Labels']['Description']}<sup>*</sup>:</label>
                </td>
                <td>
                      <input tabindex=6  required class="text required" type="text" id="Description" name="Description" value="" />
                </td>
                
            </tr>
            
             <tr>
                <td>
               <label class="cardLabel" for="ServiceProviderSupplierID">{$page['Labels']['Supplier']}<sup>*</sup>:</label>
		</td>
               <td>
                  <select  required class="text" id="ServiceProviderSupplierID" name="ServiceProviderSupplierID" >
			    <option value="">{$page['Labels']['SelectSupplier']}</option>
			    {foreach $suppliers as $code}
				<option value="{$code.ServiceProviderSupplierID}">{$code.CompanyName}</option>
			    {/foreach}
		    </select>
                </td>
                <td>
                  <label class="cardLabel" for="ServiceProviderColourID" >{$page['Labels']['Colour']}:</label>
                </td>
                <td>
                     <select  required name="ServiceProviderColourID" id="ServiceProviderColourID" class="text" >
                                <option value="" >{$page['Labels']['SelectColour']}</option>

                                
                                {foreach $colours as $c}

                                    <option value="{$c.ServiceProviderColourID}" >{$c.ColourName|escape:'html'}</option>
                                    
                                {/foreach}
                                
                            </select>
                </td>
            </tr>
            
             <tr>
                <td>
                <label required class="cardLabel "  for="PartNumber">{$page['Labels']['StockNo']}<sup>*</sup>:</label>
		</td>
               <td>
                  <input tabindex=3  class="text uppercaseText" type="text" id="PartNumber" name="PartNumber" value="" />
                </td>
                <td>
                 <label  class="cardLabel" for="MinStock" >{$page['Labels']['MinStock']}:</label>
                </td>
                <td>
                    <input type="hidden" name="PrimaryServiceProviderModelID" value="0">
                     <input tabindex=9 class="text"   type="text"   name="MinStock" value="" id="MinStock" >
                </td>
            </tr>
            
            
             <tr>
                <td>
              <label class="cardLabel" for="Qty">{$page['Labels']['Qty']}<sup>*</sup>:</label>
		</td>
               <td>
                     <input tabindex=4 required class="text" type="text" id="qty" name="qty" value="" />
                </td>
                 <td>
                 <label  class="cardLabel" for="MakeUpTo" >{$page['Labels']['MakeUpTo']}:</label>
                </td>
                <td>
                      <input tabindex=10 class="text"   type="text"   name="MakeUpTo" value="" id="MakeUpTo" >
                </td>
            </tr>
            
            
             <tr>
                <td>
             <label class="cardLabel" for="PurchaseCost">{$page['Labels']['CostEach']}<sup>*</sup>:</label>
		</td>
               <td>
                     <input tabindex=5 required class="text" type="text" id="PurchaseCost" name="PurchaseCost" value="" />
                </td>
               <td>
               
                </td>
                <td>
               
                </td>
            </tr>
            
            
             <tr>
                 <td colspan=4 style="text-align:center!important;">
            
                  
              <div style="float:right">
                 <button   style="width:100px;float:right;margin-top:30px;" onclick="receiveStockToggle();$('#receiveStockForm')[0].reset()" id="CancelButton" type="button" class="btnCancel">{$page['Buttons']['Cancel']}</button>
             </div>        
                     
             <div style="float:right;margin-right: 270px">
                          <span id="loadergif" style="display: none" class="blueText" >
		    <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >
		</span>
                 <button tabindex=11 style="width:100px" onclick="insertStockPart()"  id="NextStockButton" type="button"  class="btnConfirm">{$page['Buttons']['Next']}</button><br>
                 <button tabindex=12  style="width:100px;float:right" onclick="insertStockPart(1);" id="ReceivingFinishButton" type="button" class="gplus-blue">{$page['Buttons']['Finish']}</button>
             
             </div>
            
            
                </td>
               
            </tr>
        
        
        </table>
        </form>
        
        
    
     
        
        
       
    
    </fieldset>    
    
    </div>
                
    <div id="view3" style="display:none;">
        <fieldset>
            <legend>Order History</legend>
            <div style="float:left">
            <label  style="width:120px;font-size:12px">Order Number:</label><span id="orderHistoryNumber"></span><br>
            <label style="width:120px;font-size:12px">Supplier:</label><span id="orderHistorySupplier"></span><br>
            
            </div>
            <div style="float:right">
                <label style="width:120px;font-size:12px">Received By:</label><span id="orderHistoryReceivedBy"></span><br>
                <label style="width:120px;font-size:12px">Date:</label><span id="orderHistoryReceivedDate"></span><br>
            </div>
           
            <table id="stockHistoryTable" border="0" cellpadding="0" cellspacing="0" class="browse">
                <thead>
                    
                <tr>
                  
                    <th>Stock Number</th>
                    <th>Description</th>
                    <th>Qty Received</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
          
            <br>
              <button  style="width:100px;float:right" onclick="receiveStockHistoryFinish();" id="ReceivingFinishButton" type="button" class="gplus-blue">Finish</button>
        </fieldset>
                
    </div>
    
</div>
                 
 
                          
                        

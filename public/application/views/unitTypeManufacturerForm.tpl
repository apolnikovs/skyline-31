{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
       
              
        
    $(document).ready(function() {
        $("#ManufacturerID").combobox();
        $("#UnitTypeID").combobox();
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="UnitTypesFormPanel" class="SystemAdminFormPanel" >
    
                <form id="UnitTypesForm" name="UnitTypesForm" method="post" action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                         <p>
                               <label ></label>
                               <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                         </p>
                       
                         <input type="hidden" name="UnitTypeManufacturerID" id="UnitTypeManufacturerID" value="{$datarow.UnitTypeManufacturerID}">                         
                         <p>
                            
                             <label class="fieldLabel" for="Manufacturer" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                           
                             <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if $datarow.UnitTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if $datarow.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                         
                         <p>
                            
                             <label class="fieldLabel" for="UnitTypeID" >{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                           
                             <select name="UnitTypeID" id="UnitTypeID" class="text" >
                                <option value="" {if $datarow.UnitTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $unitTypes as $unitType}

                                    <option value="{$unitType.UnitTypeID}" {if $datarow.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                         
                          <p>
                            
                             <label class="fieldLabel" for="ProductGroup" >{$page['Labels']['product_group']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                           
                             <input id="ProductGroup" name="ProductGroup" type="text" value="{$datarow.ProductGroup}">

                          </p>
                          
                             
                        <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; 

                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                        </p>
                            
                         

                        <p>

                            <span class= "bottomButtons" >



                                {if $datarow.UnitTypeID neq '' && $datarow.UnitTypeID neq '0'}

                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {else}

                                    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['insert']|escape:'html'}" >

                                {/if}

                                    <br>
                                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                            </span>

                        </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
